package com.coinbase;

import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.coinbase.api.Coinbase;
import com.coinbase.api.CoinbaseBuilder;
import com.coinbase.api.exception.CoinbaseException;

public class test {
    public static void main(String[] args)
            throws KeyManagementException, NoSuchAlgorithmException, IOException, CoinbaseException {
        SSLContext sslContext = SSLContext.getInstance("SSL");

        // set up a TrustManager that trusts everything
        sslContext.init(null, new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                System.out.println("getAcceptedIssuers =============");
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                System.out.println("checkClientTrusted =============");
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                System.out.println("checkServerTrusted =============");
            }
        } }, new SecureRandom());

        // Coinbase cbb = new CoinbaseBuilder().withBaseApiURL(null).build();
        Coinbase cbb = new CoinbaseBuilder().withBaseApiURL(new URL("https://api-public.sandbox.pro.coinbase.com"))
                .withSSLContext(sslContext).build();

        System.out.println(cbb.getSupportedCurrencies());

    }
}
